package com.cepprice.androidversions;

public class AndroidInfo {

    private String mVersionTitle;

    private String mVersionNumber;

    private int mImageResourceId;

    public AndroidInfo(String versionTitle, String versionNumber, int imageRosourceId) {
        mVersionTitle = versionTitle;
        mVersionNumber = versionNumber;
        mImageResourceId = imageRosourceId;
    }

    public String getVersionName() {
        return mVersionTitle;
    }

    public String getVersionNumber() {
        return mVersionNumber;
    }

    public int getImageResourceId() {
        return mImageResourceId;
    }
}
