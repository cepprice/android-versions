package com.cepprice.androidversions;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ArrayList<String> links = new ArrayList<>();
        links.add("https://ru.wikipedia.org/wiki/Android_Donut");
        links.add("https://ru.wikipedia.org/wiki/Android_Eclair");
        links.add("https://ru.wikipedia.org/wiki/Android_Froyo");
        links.add("https://ru.wikipedia.org/wiki/Android_GingerBread");
        links.add("https://ru.wikipedia.org/wiki/Android_Honeycomb");
        links.add("https://ru.wikipedia.org/wiki/Android_Ice_Cream_Sandwich");
        links.add("https://ru.wikipedia.org/wiki/Android_Jelly_Bean");
        links.add("https://ru.wikipedia.org/wiki/Android_KitKat");
        links.add("https://ru.wikipedia.org/wiki/Android_Lollipop");
        links.add("https://ru.wikipedia.org/wiki/Android_Marshmallow");


        ArrayList<AndroidInfo> versions = new ArrayList<>();
        versions.add(new AndroidInfo("Donut", "1.6", R.drawable.donut));
        versions.add(new AndroidInfo("Eclair", "2.0-2.1", R.drawable.eclair));
        versions.add(new AndroidInfo("Froyo", "2.2-2.2.3", R.drawable.froyo));
        versions.add(new AndroidInfo("GingerBread", "2.3-2.3.7", R.drawable.gingerbread));
        versions.add(new AndroidInfo("Honeycomb", "3.0-3.2.6", R.drawable.honeycomb));
        versions.add(new AndroidInfo("Ice Cream Sandwich", "4.0-4.0.4", R.drawable.icecream));
        versions.add(new AndroidInfo("Jelly Bean", "4.1-4.3.1", R.drawable.jellybean));
        versions.add(new AndroidInfo("KitKat", "4.4-4.4.4", R.drawable.kitkat));
        versions.add(new AndroidInfo("Lollipop", "5.0-5.1.1", R.drawable.lollipop));
        versions.add(new AndroidInfo("Marshmallow", "6.0-6.0.1", R.drawable.marshmallow));

        AndroidListAdapter adapter = new AndroidListAdapter(this, versions);

        ListView listView = findViewById(R.id.android_list);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                Uri adress = Uri.parse(links.get(pos));
                Intent goToWiki = new Intent(Intent.ACTION_VIEW, adress);
                startActivity(goToWiki);
            }
        });
    }
}
